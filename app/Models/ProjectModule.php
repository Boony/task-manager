<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $project_id
 * @property integer $parent_id
 * @property string $name
 * @property string $priority
 * @property integer $status
 * @property integer $score
 * @property string $created_at
 * @property string $updated_at
 * @property Project $project
 * @property ProjectModule $parent
 */
class ProjectModule extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'project_id', 'parent_id', 'priority', 'status', 'score', 'created_at', 'updated_at'
    ];

    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    public function parent()
    {
        return $this->hasOne(ProjectModule::class, 'id', 'parent_id');
    }

    public static function enumItems()
    {
        return [
            ['value' => 'low', 'name' => 'Low'],
            ['value' => 'medium', 'name' => 'Medium'],
            ['value' => 'high', 'name' => 'High']
        ];
    }
}
