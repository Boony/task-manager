<?php


namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $creator_id
 * @property integer $executor_id
 * @property integer $module_id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property string $priority
 * @property integer $status
 * @property integer $score
 * @property integer $order
 * @property string $created_at
 * @property string $updated_at
 * @property ProjectModule $module
 * @property User $executor
 * @property User $creator
 */
class Task extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'url', 'module_id', 'creator_id', 'executor_id', 'priority', 'status', 'score',
        'order', 'created_at', 'updated_at'
    ];

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'creator_id');
    }

    public function executor()
    {
        return $this->hasOne(User::class, 'id', 'executor_id');
    }

    public function module()
    {
        return $this->hasOne(ProjectModule::class, 'id', 'module_id');
    }

    public function scheduler()
    {
        return $this->hasOne(TaskScheduler::class, 'task_id', 'id')
            ->whereNull('task_schedulers.spend_minutes');
    }

    public static function enumItems()
    {
        return [
            ['value' => 'low', 'name' => 'Low'],
            ['value' => 'medium', 'name' => 'Medium'],
            ['value' => 'high', 'name' => 'High']
        ];
    }
}
