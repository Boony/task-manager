<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property integer $id
 * @property string $name
 * @property integer $score
 * @property string $priority
 * @property string $finish_date
 * @property string $image
 * @property integer $work_days
 * @property string $created_at
 * @property string $updated_at
 * @property ProjectModule $modules
 */
class Project extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'score', 'priority', 'finish_date', 'image', 'work_days', 'created_at', 'updated_at'
    ];

    public function modules()
    {
        return $this->hasMany(ProjectModule::class, 'project_id', 'id')
            ->orderBy('project_modules.id');
    }

    public static function enumItems()
    {
        return [
            ['value' => 'low', 'name' => 'Low'],
            ['value' => 'medium', 'name' => 'Medium'],
            ['value' => 'high', 'name' => 'High']
        ];
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            return $file->store('project', 'public');
        }
        return $this->image;
    }
}
