<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\ProjectModule;
use Illuminate\Http\Request;

class ProjectModuleController extends Controller
{
    public function index()
    {
        $modules = ProjectModule::query()
            ->orderBy('id')
            ->paginate(15);
        return success_out($modules, true);
    }

    public function list()
    {
        $modules = ProjectModule::query()->orderBy('id')->get();
        return success_out($modules);
    }

    public function create(Request $request)
    {
        $data = $request->validate([
            'project_id' => 'required|exists:projects,id',
            'parent_id' => 'nullable',
            'name' => 'required',
            'status' => 'required',
            'priority' => 'required',
            'score' => 'required|integer|min:1|max:10',
        ]);

        $model = new ProjectModule();
        $model->fill($data);

        \DB::beginTransaction();
        try {
            if ($model->save()) {
                \DB::commit();
                return success_out([]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return error_out([], 422, $e->getMessage());
        }
        return error_out([], 422, 'Ошибка во время сохранение');

    }

    public function update(Request $request, ProjectModule $module)
    {
        $data = $request->validate([
            'project_id' => 'required|exists:projects,id',
            'parent_id' => 'nullable',
            'name' => 'required',
            'status' => 'required',
            'priority' => 'required',
            'score' => 'required|integer|min:1|max:10',
        ]);
        \DB::beginTransaction();
        try {
            if ($module->update($data)) {
                \DB::commit();
                return success_out([]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return error_out([], 422, $e->getMessage());
        }
        return error_out([], 422, 'Ошибка во время сохранение');
    }

    public function edit(ProjectModule $module)
    {
        $module->project;
        $module->parent;
        return success_out($module);
    }

    public function delete(ProjectModule $module)
    {
        if ($module->delete()) {
            return success_out([]);
        } else {
            return error_out([], 422, 'Can`t delete');
        }
    }

}
