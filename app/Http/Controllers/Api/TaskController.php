<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\TaskLog;
use App\Models\TaskScheduler;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::query()
            ->with('scheduler')
            ->orderBy('order')
            ->paginate(15);
        return success_out($tasks, true);
    }

    public function list()
    {
        $tasks = Task::query()
            ->with('scheduler')
            ->orderBy('order')
            ->get();
        return success_out($tasks);
    }

    public function edit(Task $task)
    {

        $task->module;
        $task->creator;
        $task->executor;

        return success_out($task);
    }

    public function create(Request $request)
    {
        $data = $request->validate([
            'module_id' => 'required|exists:project_modules,id',
            'executor_id' => 'nullable|exists:users,id',
            'title' => 'required',
            'description' => 'nullable',
            'url' => 'nullable',
            'score' => 'required|integer|min:1|max:10',
            'priority' => 'required',
            'order' => 'required',
            'status' => 'required',
        ]);
        $data['creator_id'] = auth()->user()->id;
        $task = new Task();
        $task->fill($data);
        \DB::beginTransaction();
        try {
            if ($task->save()) {
                self::log($data, $task);
                \DB::commit();
                return success_out([]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return error_out([], 422, $e->getMessage());
        }
        return error_out([], 422, 'Ошибка во время сохранение');
    }

    public function update(Request $request, Task $task)
    {
        $data = $request->validate([
            'module_id' => 'required|exists:project_modules,id',
            'executor_id' => 'nullable|exists:users,id',
            'title' => 'required',
            'description' => 'nullable',
            'url' => 'nullable',
            'score' => 'required|integer|min:1|max:10',
            'priority' => 'required',
            'order' => 'required',
            'status' => 'required',
        ]);
        $data['creator_id'] = auth()->user()->id;
        $temp = $task->toArray();
        try {
            $task->update($data);
            self::log($temp, $task);
            return success_out([]);

        } catch (\ErrorException $e) {
            return error_out($e->getMessage());
        }
    }

    public function delete(Task $task)
    {
        $temp = $task->toArray();
        if ($task->delete()) {
            self::log($temp, null);
            return success_out([]);
        } else {
            return error_out([], 422, 'Can`t delete');
        }
    }

    public function start(Task $task)
    {
        $scheduler = new TaskScheduler();
        $scheduler->fill([
            'user_id' => auth()->user()->id,
            'task_id' => $task->id,
            'started_at' => date('Y-m-d H:i:s')
        ]);
        if ($scheduler->save()) {
            return success_out($scheduler);
        } else {
            return error_out([], 422, 'Ошибка во время сохранение');
        }

    }

    public function pause(TaskScheduler $scheduler)
    {
        $date = date('Y-m-d H:i:s');
        $minutes = round(abs(strtotime($date) - strtotime($scheduler->started_at)) / 60);
        $scheduler->update([
            'spend_minutes' => $minutes,
        ]);
        return success_out($scheduler);
    }

    public function schedulers(Task $task)
    {
        $schedulers = TaskScheduler::query()->where('task_id', $task->id)->get();
        return success_out($schedulers);
    }

    private function log($data, $model)
    {
        $log = new TaskLog();
        $log->fill([
            'user_id' => auth()->user()->id,
            'before' => $data ? json_encode($data) : null,
            'after' => $model ? json_encode($model->toArray()) : null,
            'task_id' => $model ? $model->id : $data['id']
        ]);
        $log->save();
    }
}
