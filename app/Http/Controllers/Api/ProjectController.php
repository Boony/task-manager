<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::query()
            ->orderBy('id')
            ->paginate(15);
        return success_out($projects, true);
    }

    public function list()
    {
        $projects = Project::query()->orderBy('id')->get();
        return success_out($projects);
    }

    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|unique:projects,name',
            'score' => 'required|integer|min:1|max:10',
            'priority' => 'required',
            'finish_date' => 'nullable',
            'image' => 'nullable',
            'work_days' => 'nullable|integer',
        ]);
        $project = new Project();
        $data['image'] = $project->upload($request);
        $project->fill($data);
        try {
            if ($project->save()) {
                return success_out([]);
            } else {
                return error_out([], 422, 'Ошибка во время сохранение');
            }
        } catch (\ErrorException $e) {
            return error_out($e->getMessage());
        }
    }

    public function edit(Project $project)
    {
        $project->modules;
        return success_out($project);
    }

    public function update(Request $request, Project $project)
    {
        $data = $request->validate([
            'name' => 'required|unique:projects,name,' . $project->id,
            'score' => 'required|integer|min:1|max:10',
            'priority' => 'required',
            'finish_date' => 'nullable',
            'image' => 'nullable',
            'work_days' => 'nullable|integer',
        ]);
        try {
            $data['image'] = $project->upload($request);
            $project->update($data);
            return success_out([]);

        } catch (\ErrorException $e) {
            return error_out($e->getMessage());
        }
    }

    public function delete(Project $project)
    {
        if ($project->delete()) {
            return success_out([]);
        }
        return error_out([], 422, 'Can`t delete');

    }
}
