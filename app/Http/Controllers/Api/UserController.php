<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::query()->paginate(15);
        return success_out($users, true);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function list()
    {
        $users = User::all();
        return success_out($users);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|unique:users',
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'role' => 'required|exists:roles,id'
        ]);
        $data['password'] = \Hash::make($data['password']);
        $user = new User();
        $user->fill($data);
        \DB::beginTransaction();
        try {
            if ($user->save()) {
                $role = Role::findById($data['role']);
                $user->assignRole($role);
                \DB::commit();
                return success_out([]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return error_out([], 422, $e->getMessage());
        }
        return error_out([], 422, 'Ошибка во время сохранение');
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return success_out($user);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->validate([
            'phone' => 'required|unique:users,phone,' . $user->id,
            'email' => 'required|email|unique:users,email,' . $user->id,
            'name' => 'required',
            'role' => 'required'
        ]);

        $user->update($data);
        $role = Role::findById($data['role']);
        $user->syncRoles($role);

        return success_out($user);
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function delete(User $user)
    {
        $user->delete();
        return success_out([]);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function roles()
    {
        $roles = Role::all();
        return success_out($roles);
    }
}
