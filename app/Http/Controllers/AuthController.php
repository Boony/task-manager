<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return ResponseFactory|JsonResponse|Response
     */
    public function login(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'password' => 'required|min:6'
        ]);

        $data = request(['phone', 'password']);

        $data['phone'] = clearPhone($data['phone']);


        $user = User::query()
            ->where('phone', $data['phone'])
            ->first();

        if (!$user) {
            return error_out(['phone' => ['Пользователь заблокирован или нет']]);
        }

        if (!auth('api')->validate($data)) {
            return error_out(['phone' => ['Неверный логин или пароль']]);
        }

        $token = auth('api')->login($user);

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User
     *
     * @return JsonResponse
     */
    public function me()
    {
        return response()->json(\auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return JsonResponse
     */
    public function logout()
    {
        \auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(\auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'permissions' => \Arr::pluck(auth('api')->user()->getAllPermissions(), 'name'),
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return string
     */
    public function guard()
    {
        return 'api';
    }
}
