<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('module_id')->index();
            $table->unsignedBigInteger('creator_id')->index();
            $table->unsignedBigInteger('executor_id')->index()->nullable();
            $table->string('title', 255);
            $table->longText('description')->nullable();
            $table->string('url', 255)->nullable();
            $table->smallInteger('score')->default(1);
            $table->enum('priority', ['low', 'medium', 'high'])->default('low');
            $table->smallInteger('order')->default(1000);
            $table->smallInteger('status')->default(0);
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('executor_id')->references('id')->on('users');
            $table->foreign('module_id')->references('id')->on('project_modules');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropIndex(['module_id']);
            $table->dropIndex(['creator_id']);
            $table->dropIndex(['executor_id']);

            $table->dropForeign(['creator_id']);
            $table->dropForeign(['executor_id']);
            $table->dropForeign(['module_id']);
        });

        Schema::dropIfExists('tasks');
    }
}
