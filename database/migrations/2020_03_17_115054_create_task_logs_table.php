<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('task_id')->index();
            $table->json('before')->nullable();
            $table->json('after')->nullable();
            $table->timestamps();

//            $table->foreign('user_id')->references('id')->on('users');
//            $table->foreign('task_id')->references('id')->on('tasks');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_logs', function (Blueprint $table) {
            $table->dropIndex(['user_id']);
            $table->dropIndex(['task_id']);

//            $table->dropForeign(['user_id']);
//            $table->dropForeign(['task_id']);
        });

        Schema::dropIfExists('task_logs');
    }
}
