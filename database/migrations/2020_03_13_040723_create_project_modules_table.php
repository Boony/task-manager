<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_modules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id')->index();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('name', 255);
            $table->smallInteger('status')->default(0);
            $table->enum('priority', ['low', 'medium', 'high'])->default('low');
            $table->smallInteger('score')->default(1);
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_modules', function (Blueprint $table) {
            $table->dropIndex(['project_id']);
            $table->dropForeign(['project_id']);
        });
        Schema::dropIfExists('project_modules');
    }
}
