<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::query()->create([
            'name' => 'Super Admin',
            'email' => 'superAdmin@gmail.com',
            'phone' => '998970000001',
            'password' => Hash::make('admin12345')
        ]);
        \App\User::query()->create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'phone' => '998970000002',
            'password' => Hash::make('admin12345')
        ]);
    }
}
