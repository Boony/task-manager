<?php

use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::query()->create([
            'name' => 'superAdmin',
            'guard_name' => 'api'
        ]);
        \App\Models\Role::query()->create([
            'name' => 'admin',
            'guard_name' => 'api'
        ]);
    }
}
