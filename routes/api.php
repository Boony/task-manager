<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('auth/login', 'AuthController@login');

    Route::group(['middleware' => 'auth.jwt'], function () {
        Route::group(['prefix' => 'auth'], function () {
            Route::post('logout', 'AuthController@logout');
            Route::post('refresh', 'AuthController@refresh');
            Route::post('me', 'AuthController@me');
        });
        Route::group(['prefix' => 'user'], function () {
            Route::get('', 'Api\UserController@index');
            Route::get('list', 'Api\UserController@list');
            Route::get('roles', 'Api\UserController@roles');
            Route::get('/{user}', 'Api\UserController@edit');
            Route::delete('/{user}', 'Api\UserController@delete');
            Route::post('/{user}', 'Api\UserController@update');
            Route::post('', 'Api\UserController@create');
        });
        Route::group(['prefix' => 'project'], function () {
            Route::get('list', 'Api\ProjectController@list');
            Route::get('', 'Api\ProjectController@index');
            Route::get('/{project}', 'Api\ProjectController@edit');
            Route::delete('/{project}', 'Api\ProjectController@delete');
            Route::post('', 'Api\ProjectController@create');
            Route::post('/{project}', 'Api\ProjectController@update');
        });
        Route::group(['prefix' => 'module'], function () {
            Route::get('list', 'Api\ProjectModuleController@list');
            Route::get('', 'Api\ProjectModuleController@index');
            Route::get('/{module}', 'Api\ProjectModuleController@edit');
            Route::delete('/{module}', 'Api\ProjectModuleController@delete');
            Route::post('', 'Api\ProjectModuleController@create');
            Route::post('/{module}', 'Api\ProjectModuleController@update');
        });
        Route::group(['prefix' => 'task'], function () {
            Route::get('/list', 'Api\TaskController@list');
            Route::get('/{task}/start', 'Api\TaskController@start');
            Route::get('/{task}/schedulers', 'Api\TaskController@schedulers');
            Route::get('/pause/{scheduler}', 'Api\TaskController@pause');

            Route::get('', 'Api\TaskController@index');
            Route::post('', 'Api\TaskController@create');
            Route::get('/{task}', 'Api\TaskController@edit');
            Route::post('/{task}', 'Api\TaskController@update');
            Route::delete('/{task}', 'Api\TaskController@delete');
        });
    });

});
